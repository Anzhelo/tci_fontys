import Course.Course;
import Course.CourseDateException;
import School.CourseDuplicateException;
import School.CourseException;
import School.School;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SchoolTests {
    private School school;

    @Before
    public void setUp() {
        school = new School("Some name", null);
    }

    @Test
    public void canNameAndDateBeNull() {
        School nullSchool = new School(null, null);
        Assert.assertTrue("Name is null", nullSchool.getName().equals("No name"));
        Assert.assertFalse("Date is null", nullSchool.getOpenningDate() == null);
    }

    @Test
    public void canNameAndDateBeSetBySetMethods() {
        school.setName(null);
        school.setOpenningDate(null);
        Assert.assertFalse("Name is null", school.getName() == null);
        Assert.assertFalse("Date is null", school.getOpenningDate() == null);
    }

    @Test
    public void courseCanBeAdded() {
        try {
            Date date = addNumberOfDaysToDate(new Date(), 1);
            Course course = new Course("New course", date, addNumberOfDaysToDate(date,1));

            school.addCourse(course);

            Assert.assertTrue("Created course is not in the list of courses.", school.getCourses().contains(course));
        } catch (CourseDateException | CourseDuplicateException | CourseException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = CourseDateException.class)
    public void courseWithEndDateBeforeStartDateToBeAddedThrowsException() throws CourseDateException {
        try {
            Date date = addNumberOfDaysToDate(new Date(), 1);

            Course course = new Course("New course", date, new Date());

            school.addCourse(course);

            Assert.assertFalse("Created course is in the list of courses.", school.getCourses().contains(course));
        } catch (CourseDuplicateException | CourseException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = CourseException.class)
    public void courseWithStartDateBeforeSchoolOpenDayToBeAddedThrowsException() throws CourseException {
        try {
            Date date = addNumberOfDaysToDate(new Date(), 1);
            School newSchool = new School("Some name", date);

            Course course = new Course("New course", new Date(), date);

            newSchool.addCourse(course);
            Assert.assertTrue("The course is added", newSchool.getCourses().contains(course));
        } catch (CourseDuplicateException | CourseDateException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = CourseDuplicateException.class)
    public void courseWithSameNameAddedThrowsException() throws CourseDuplicateException{
        try{
            Course course = new Course("ACourse",new Date(),addNumberOfDaysToDate(new Date(),1));
            school.addCourse(course);
            school.addCourse(course);
            Assert.assertTrue("There are 2 courses with the same name", school.getCourses().size() == 1);
        }catch(CourseException | CourseDateException e){
            e.printStackTrace();
        }
    }

    @Test
    public void getCourseByName(){
        try {
            String courseName = "SomeName";
            Course course = new Course(courseName,new Date(),addNumberOfDaysToDate(new Date(),1));

            school.addCourse(course);

            Assert.assertTrue("Course names are not the same", school.getCourseByName(courseName) == course);
        }catch (CourseException | CourseDateException | CourseDuplicateException e){
            e.printStackTrace();
        }

    }

    @Test
    public void getCourseByNameWhenThereIsNoSuchCourse(){
        try {
            String courseName = "SomeName";
            Course course = new Course(courseName,new Date(),addNumberOfDaysToDate(new Date(),1));

            school.addCourse(course);

            Assert.assertFalse("Course names are not the same", school.getCourseByName("SomeOtherName") == course);
        }catch (CourseException | CourseDateException | CourseDuplicateException e){
            e.printStackTrace();
        }
    }

    @Test
    public void getListOfAllCourseNames() {
        try {
            List<String> listOfNames = new ArrayList<>();
            listOfNames.add("CourseA");
            listOfNames.add("CourseB");
            listOfNames.add("CourseC");
            Course course1 = new Course(listOfNames.get(0), new Date(), addNumberOfDaysToDate(new Date(), 1));
            Course course2 = new Course(listOfNames.get(1), new Date(), addNumberOfDaysToDate(new Date(), 1));
            Course course3 = new Course(listOfNames.get(2), new Date(), addNumberOfDaysToDate(new Date(), 1));

            school.addCourse(course1);
            school.addCourse(course2);
            school.addCourse(course3);

            Assert.assertArrayEquals(listOfNames.toArray(), school.getAllCoursesNames().toArray());
        }catch (CourseDateException | CourseDuplicateException | CourseException e) {
            e.printStackTrace();
        }
    }

//    @Test
//    public void getListOfCourseCopies(){
//        try{
//            List<String> listOfNames = new ArrayList<>();
//            listOfNames.add("CourseA");
//            listOfNames.add("CourseB");
//            listOfNames.add("CourseC");
//            Course course1 = new Course(listOfNames.get(0), new Date(), addNumberOfDaysToDate(new Date(), 1));
//            Course course2 = new Course(listOfNames.get(1), new Date(), addNumberOfDaysToDate(new Date(), 1));
//            Course course3 = new Course(listOfNames.get(2), new Date(), addNumberOfDaysToDate(new Date(), 1));
//
//            school.addCourse(course1);
//            school.addCourse(course2);
//            school.addCourse(course3);
//            List<Course> copies = school.getCopiesOfCourses();
//            Assert.assertFalse("Courses are the same", course1 == copies.get(0));
//        }catch (CourseException | CourseDateException | CourseDuplicateException e) {
//            e.printStackTrace();
//        }
//    }

    private Date addNumberOfDaysToDate(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }
}
