public class MyFirstClass {
    private int exampleInteger;
    private String exampleText;
    private double exampleDouble;


    public MyFirstClass(){
        exampleDouble = 5.3;
    }

    public MyFirstClass(int exampleInteger,String exampleText, double exampleDouble){
        setExampleInteger(exampleInteger);
        setExampleText(exampleText);
        this.exampleDouble = exampleDouble;
    }

    public int getExampleInteger() {
        return exampleInteger;
    }

    public void setExampleInteger(int exampleInteger) {
        this.exampleInteger = exampleInteger;
    }

    public String getExampleText() {
        return exampleText;
    }

    public void setExampleText(String exampleText) {
        this.exampleText = exampleText;
    }

    public double getExampleDouble() {
        return exampleDouble;
    }
}
