# TCI_Fontys

## Test coverage week 1 : https://anzhelo.gitlab.io/tci_fontys/week1_test_jacoco/

## Test report week 1 : https://anzhelo.gitlab.io/tci_fontys/week1_test_report/

## Test coverage week 2 : https://anzhelo.gitlab.io/tci_fontys/week2_test_jacoco/

## Test report week 2 : https://anzhelo.gitlab.io/tci_fontys/week2_test_report/

## Test coverage week 3 : https://anzhelo.gitlab.io/tci_fontys/week3_test_jacoco/

## Test report week 3 : https://anzhelo.gitlab.io/tci_fontys/week3_test_report/