import Course.Course;
import Course.CourseDateException;
import School.CourseDuplicateException;
import School.CourseException;
import School.School;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.mockito.Mockito.*;

public class SchoolMockTests {
    private School school;

    @Before
    public void setUp() {
        school = new School("Some name", null);
    }

    @Test
    public void canSchoolNameBeMocked() {
        school = mock(School.class);
        when(school.getName()).thenReturn("MockName");
        Assert.assertTrue("Name is not mocked", school.getName().equals("MockName"));
    }

    @Test
    public void canMockedCourseCanBeAdded() {
        try {
            Date date = addNumberOfDaysToDate(new Date(), 1);
            Course course = mock(Course.class);
            when(course.getName()).thenReturn("SomeCourseName");
            when(course.getStartDate()).thenReturn(date);

            school.addCourse(course);

            Assert.assertTrue("Created course is not in the list of courses.", school.getCourses().contains(course));
        } catch ( CourseDuplicateException | CourseException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = CourseDateException.class)
    public void canMockedCourseWithEndDateBeforeStartDateToBeAddedThrowException() {
        try {
            Course course = mock(Course.class);
            when(course.getStartDate()).thenThrow(CourseDateException.class);

            school.addCourse(course);

            Assert.assertFalse("Created course is in the list of courses.", school.getCourses().contains(course));
        } catch (CourseDuplicateException | CourseException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = CourseException.class)
    public void canMockedCourseWithStartDateBeforeSchoolOpenDayToBeAddedThrowException() throws CourseException {
        try {
            Date date = addNumberOfDaysToDate(new Date(), 1);
            School newSchool = new School("Some name", date);

            Course course = mock(Course.class);
            when(course.getStartDate()).thenReturn(new Date());

            newSchool.addCourse(course);
            Assert.assertTrue("The course is added", newSchool.getCourses().contains(course));
        } catch (CourseDuplicateException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void verifyThatCourseStartDateIsCalledWhenAddingCourseToSchool(){
        try{
            Date date = addNumberOfDaysToDate(new Date(), 1);
            Course course = mock(Course.class);
            when(course.getName()).thenReturn("Course");
            when(course.getStartDate()).thenReturn(new Date());
            when(course.getEndDate()).thenReturn(date);

            school.addCourse(course);

            verify(course,atLeastOnce()).getStartDate();
            Assert.assertTrue("Course was not added", school.getCourses().size() == 1);
        }catch(CourseException | CourseDuplicateException e){
            e.printStackTrace();
        }
    }

    private Date addNumberOfDaysToDate(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }
}
