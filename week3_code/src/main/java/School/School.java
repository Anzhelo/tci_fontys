package School;

import Course.Course;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class School {
    private String name;
    private Date openningDate;
    private List<Course> courses;

    public School(String name, Date openningDate){
        this.name = name;
        this.openningDate = openningDate;
        courses = new ArrayList<>();
        if(this.name == null)
            this.name = "No name";
        if(this.openningDate == null)
            this.openningDate = new Date();
    }

    public void setName(String name) {
        if(name == null)
            return;
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setOpenningDate(Date openningDate){
        if(openningDate == null)
            return;
        this.openningDate = openningDate;
    }

    public Date getOpenningDate() {
        return openningDate;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void addCourse(Course course) throws CourseException, CourseDuplicateException {
        if(course.getStartDate().before(this.openningDate))
            throw new CourseException();
        for(Course c : courses)
            if(c.getName().equals(course.getName()))
                throw  new CourseDuplicateException();
        courses.add(course);
    }

    public Course getCourseByName(String name){
        for(Course c : courses)
            if(c.getName() == name)
                return c;
        return null;
    }

    public List<String> getAllCoursesNames(){
        List<String> output = new ArrayList<>();
        for(Course c : courses)
            output.add(c.getName());
        return output;
    }

    public List<Course> getCopiesOfCourses(){
        return courses;
    }

    public void removeCourseAtIndex(int index){
        courses.remove(index);
    }

}
