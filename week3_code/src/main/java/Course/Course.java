package Course;

import java.util.Date;

public class Course {
    private String name;
    private Date startDate;
    private Date endDate;

    public Course(String name, Date startDate, Date endDate) throws CourseDateException {
        this.name = name;
        this.startDate = startDate;
        if(startDate.after(endDate)) {
            throw new CourseDateException();
        }
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) throws CourseDateException {
        if(endDate.before(startDate)){
            throw new CourseDateException();
        }
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) throws CourseDateException {
        if(startDate.after(endDate)){
            throw new CourseDateException();
        }
        this.endDate = endDate;
    }
}
