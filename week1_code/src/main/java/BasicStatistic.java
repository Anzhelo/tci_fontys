import java.util.ArrayList;
import java.util.List;

/**
 * very simple implementation of the BasicStatisticInterface
 */
public class BasicStatistic implements BasicStatisticInterface {

    private List<Double> dataList;

    public BasicStatistic(){
        dataList = new ArrayList<Double>();
    }
    @Override
    public void addDoubleToData(Double valueToAdd){
        dataList.add(valueToAdd);
    }

    @Override
    public void clearData(){
        dataList.clear();
    }
	
    @Override
    public int numberOfDataItems(){
        return dataList.size();
    }

    @Override
    public Double sum(){
        double sum = 0.0;
        for(Double d:dataList)
            sum += d;
        return sum;
    }

    @Override
    public Double highestValue() throws NoDataItemsException {
        double highest = 0;
        if(dataList.size() == 0)
            throw new NoDataItemsException();
        for(Double d: dataList)
            if(d  > highest)
                highest = d;
        return highest;
    }

    @Override
    public Double getMean() throws NoDataItemsException {
        if(dataList.size() == 0)
            throw new NoDataItemsException();
        return sum() / dataList.size();
    }

    @Override
    public double getMedian() throws NoDataItemsException {
        double rv = 0.0;
        return rv;
    }

    @Override
    public double getStandardDeviation() throws NoDataItemsException {
        double rv = 0.0;
        return rv;
    }
}
