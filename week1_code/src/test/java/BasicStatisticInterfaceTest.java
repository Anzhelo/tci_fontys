import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BasicStatisticInterfaceTest {

    private BasicStatistic basicStatistic;

    @Before
    public void setUp() throws Exception {
        basicStatistic = new BasicStatistic();
    }

    @Test
    public void areDataItemsAvailableWhenNotAdded(){
        Assert.assertTrue("Data items were not 0",basicStatistic.numberOfDataItems() == 0);
    }

    @Test
    public void isDataCleared(){
        basicStatistic.clearData();
        Assert.assertTrue("Data is not cleared",basicStatistic.numberOfDataItems() == 0);
    }

    @Test
    public void addDoubleToData() {
        basicStatistic.addDoubleToData(10.3);
        Assert.assertTrue("Data is not added", basicStatistic.numberOfDataItems() != 0);
    }

    @Test
    public void addingMultipleDataItems(){
        int dataItemsToAdd = 10;
        for(int i=0; i<dataItemsToAdd; i++){
            basicStatistic.addDoubleToData(10.3);
        }
        Assert.assertTrue("The number of added items is not equal to the expected number",basicStatistic.numberOfDataItems() == dataItemsToAdd);
    }

    @Test
    public void sum(){
        basicStatistic.addDoubleToData(5.0);
        basicStatistic.addDoubleToData(5.0);
        Assert.assertTrue("The summary of the data items is incorrect.",basicStatistic.sum() == 10.0);
    }

    @Test
    public void isSumZero(){
        Assert.assertTrue("When there are no data items the sum is not 0.", basicStatistic.sum() == 0);
    }

    @Test(expected = NoDataItemsException.class)
    public void checkGetMeanException() throws NoDataItemsException{
        basicStatistic.getMean();
    }

    @Test
    public void getMeanFunctionality() throws NoDataItemsException{
        basicStatistic.addDoubleToData(10.0);
        basicStatistic.addDoubleToData(2.0);
        Assert.assertTrue("The average of the numbers was wrong", basicStatistic.getMean() == 6);
    }

    @Test
    public void highestValueFunctionality() throws NoDataItemsException{
        for(double i=0; i<10; i++)
            basicStatistic.addDoubleToData(i);
        Assert.assertTrue("The highest number of the data set was wrong", basicStatistic.highestValue() == 9);
    }

    @Test(expected = NoDataItemsException.class)
    public void highestValueException()throws NoDataItemsException{
        basicStatistic.highestValue();
    }
}